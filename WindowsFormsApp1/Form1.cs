﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = "Лабораторная работа №3";
            comboBox1.SelectedIndex = 0;
            textBox4.Text = "1";
            textBox5.Text = "0";
            textBox6.Text = "1";
            textBox7.Text = "0";
            textBox8.Text = "1";
            textBox9.Text = "0";
            textBox10.Text = "1";
            textBox7.Visible = false;
            textBox8.Visible = false;
            textBox9.Visible = false;
            textBox10.Visible = false;
            label7.Visible = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                textBox7.Visible = false;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;
                textBox14.Visible = false;
                textBox15.Visible = false;
                textBox16.Visible = false;
                textBox17.Visible = false;
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                textBox7.Visible = true;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;
                textBox14.Visible = true;
                textBox15.Visible = false;
                textBox16.Visible = false;
                textBox17.Visible = false;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                textBox7.Visible = true;
                textBox8.Visible = true;
                textBox9.Visible = false;
                textBox10.Visible = false;
                textBox14.Visible = true;
                textBox15.Visible = true;
                textBox16.Visible = false;
                textBox17.Visible = false;
            }
            else if (comboBox1.SelectedIndex == 3)
            {
                textBox7.Visible = true;
                textBox8.Visible = true;
                textBox9.Visible = true;
                textBox10.Visible = false;
                textBox14.Visible = true;
                textBox15.Visible = true;
                textBox16.Visible = true;
                textBox17.Visible = false;
            }
            else if (comboBox1.SelectedIndex == 4)
            {
                textBox7.Visible = true;
                textBox8.Visible = true;
                textBox9.Visible = true;
                textBox10.Visible = true;
                textBox14.Visible = true;
                textBox15.Visible = true;
                textBox16.Visible = true;
                textBox17.Visible = true;
            }
            textBox11.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";
            textBox15.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
        }

        string codeDecode(string data, string register, string tabs)
        {
            string result = "";
            foreach (char symbol in data)
            {
                result += (char)(symbol ^ Convert.ToInt32(register, 2));

                int newReg = int.Parse(register[int.Parse(tabs[0].ToString())].ToString()) ^ int.Parse(register[int.Parse(tabs[1].ToString())].ToString());
                for (int i = 2; i < tabs.Length; i++)
                {
                    newReg = newReg ^ int.Parse(register[int.Parse(tabs[i].ToString())].ToString());
                }

                register = register.Substring(1) + newReg;
            }

            return result;
        }

        string getTabs(int period)
        {
            string tabs = "";
            TextBox[] textBoxes = { textBox11, textBox12, textBox13, textBox14, textBox15, textBox16, textBox17 };
            for (int i = 0; i < period; i++)
            {
                if (textBoxes[i].Text != "")
                {
                    tabs += i;
                }
            }

            return tabs;
        }

        string printRegisters(string register, string tabs, int period)
        {
            string registerOutput = "";
            for (int j = 1; j < Math.Pow(2, period) + 1; j++)
            {
                registerOutput += j.ToString() + ": " + register + Environment.NewLine;

                int newReg = int.Parse(register[int.Parse(tabs[0].ToString())].ToString()) ^ int.Parse(register[int.Parse(tabs[1].ToString())].ToString());
                for (int i = 2; i < tabs.Length; i++)
                {
                    newReg = newReg ^ int.Parse(register[int.Parse(tabs[i].ToString())].ToString());
                }

                register = register.Substring(1) + newReg;
            }

            return registerOutput;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string register = textBox4.Text + textBox5.Text + textBox6.Text;
            int period = 3;
            if (comboBox1.SelectedIndex == 1)
            {
                register += textBox7.Text;
                period = 4;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                register += textBox7.Text + textBox8.Text;
                period = 5;
            }
            else if (comboBox1.SelectedIndex == 3)
            {
                register += textBox7.Text + textBox8.Text + textBox9.Text;
                period = 6;
            }
            else if (comboBox1.SelectedIndex == 4)
            {
                register += textBox7.Text + textBox8.Text + textBox9.Text + textBox10.Text;
                period = 7;
            }
            label7.Visible = true;
            label8.Text = (Math.Pow(2, period) - 1).ToString();

            string tabs = getTabs(period);
            string registerOutput = printRegisters(register, tabs, period);
            string result = codeDecode(textBox1.Text, register, tabs);

            textBox2.Text = result;
            textBox18.Text = registerOutput;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string register = textBox4.Text + textBox5.Text + textBox6.Text;
            int period = 3;
            if (comboBox1.SelectedIndex == 1)
            {
                register += textBox7.Text;
                period = 4;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                register += textBox7.Text + textBox8.Text;
                period = 5;
            }
            else if (comboBox1.SelectedIndex == 3)
            {
                register += textBox7.Text + textBox8.Text + textBox9.Text;
                period = 6;
            }
            else if (comboBox1.SelectedIndex == 4)
            {
                register += textBox7.Text + textBox8.Text + textBox9.Text + textBox10.Text;
                period = 7;
            }
            label7.Visible = true;
            label8.Text = (Math.Pow(2, period) - 1).ToString();

            string tabs = getTabs(period);
            string result = codeDecode(textBox2.Text, register, tabs);

            textBox3.Text = result;
        }
    }
}
